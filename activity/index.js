/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function userDetails(){
		let fullName = prompt("Enter fullname");
		let age = prompt("Enter Age");
		let address = prompt("Enter Address");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);

	};

	userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function topFiveBands(){
		console.log("1. One Ok Rock");
		console.log("2. Hige Dandism");
		console.log("3. My Chemical Romance");
		console.log("4. Aimyon");
		console.log("5. Bring Me The Horizon");
	};

	topFiveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function topMoviesOfAllTime(movieName, movieRating){
		console.log(movieName);
		console.log("Rotten Tomatoes Rating: " + movieRating + "%");
	};

	topMoviesOfAllTime("1. Saving Private Ryan", 94);	
	topMoviesOfAllTime("2. The Lord of the Rings: The Return of the King", 93);
	topMoviesOfAllTime("3. The Lord of the Rings: The Two Towers", 95);
	topMoviesOfAllTime("4. Dunkirk", 92);
	topMoviesOfAllTime("5. 1917", 88);
	
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);
